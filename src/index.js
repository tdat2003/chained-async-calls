// @flow

// alternative 1
setTimeout(() => {
  console.log('after one second');
  setTimeout(() => {
    console.log('after two seconds');
    setTimeout(() => {
      console.log('after three seconds');
    }, 1000);
  }, 1000);
}, 1000);

// alternative 2 using promises. A Promise represents a value which may be
// available now, or in the future, or never.
function sleep(duration) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, duration);
  });
}

sleep(1000)
  .then(() => {
    console.log('after one second');
    return sleep(1000);
  })
  .then(() => {
    console.log('after two seconds');
    return sleep(1000);
  })
  .then(() => {
    console.log('after three seconds');
  });

// alternative 3 using async await (must be within an async function)
async function await_example() {
  await sleep(1000);
  console.log('after one second');
  await sleep(1000);
  console.log('after two seconds');
  await sleep(1000);
  console.log('after three seconds');
}
await_example();

// await_example is actually equal to:
function example() {
  return sleep(1000)
    .then(() => {
      console.log('after one second');
      return sleep(1000);
    })
    .then(() => {
      console.log('after two seconds');
      return sleep(1000);
    })
    .then(() => {
      console.log('after three seconds');
    });
}
example();

console.log('end of script');
